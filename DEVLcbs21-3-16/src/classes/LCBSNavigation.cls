public class LCBSNavigation {

    

public User u {get;set;}
public Boolean isContractor {get;set;}
public Boolean isDistributor {get;set;}
public String BuildingUrl {get;set;}
public String HomeUrl {get;set;}
 public LCBSNavigation()
  {

     String domain= URL.getSalesforceBaseUrl().toExternalForm().remove('https://').substringBefore('-');  //LCBSParameters.DISTRIBUTOR_HOME_URL;// BuildingUrl = 'https://sggabdgbldevlcbsweb.azurewebsites.net/app/buildings';
      if (Test.isRunningTest()) {
        domain= 'devlcbs';
        }
     system.debug('%%%%Naga Testing%%%%'+domain); 
     BuildingUrl = LCBS_AzureUrls__c.getvalues(domain).LCBS_HomeUrl__c;  
     HomeUrl = LCBS_AzureUrls__c.getvalues(domain).Tenant_HomePage__c; 
     system.debug('%%%%Naga Testing%%%%'+BuildingUrl); 
     system.debug('%%%%Naga Testing%%%%'+HomeUrl); 
    u=[select id,name,account.EnvironmentalAccountType__c from user where id=:userinfo.getuserid()];
    if(u.account.EnvironmentalAccountType__c == 'Contractor')
    {
     isContractor =true;
     isDistributor = false;
    }
    else if (u.account.EnvironmentalAccountType__c == 'Distributor')
    {
     isContractor =false;
     isDistributor =true;
    }  
    else
    {
     isContractor =false;
     isDistributor =false;
    }
    

  }
  public PageReference logout() 
    {
       /*PageReference p = new PageReference('/secur/logout.jsp');
        p.setRedirect(true);
        return p; */
        PageReference p1 = new PageReference('http://login.microsoftonline.com/logout.srf');
        p1.setRedirect(true);
        return p1;
        
    } 
    
    public PageReference helpIndexPageMethod(){
     PageReference p = new pageReference('/LCBS_HelpIndexPage');
     p.setRedirect(true);
     return p;
    }
    
  
}