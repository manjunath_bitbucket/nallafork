@RestResource(urlMapping='/AlertNotification/*')
  
global class LCBSAlertNotificationRestAPI {

         global class TransactionId{
            public String Id;
            
            public TransactionId(String Id) {
                this.Id = Id;
            }
        }
        
         global class NotificationType {
            public String NotificationType;
            
            public NotificationType(String NotificationType) {
                this.NotificationType = NotificationType;
            }
        }
        
        global class NotificationStatus {
            public String Status;
            public String ErrorMessage;
            
            public NotificationStatus(String Status, String ErrorMessage) {
                this.Status = Status;
                this.ErrorMessage = ErrorMessage;
            }
        }
        
        global class AlertNotificationResponse {
              public String TransactionId;
//            public TransactionId TransactionId;
//            public NotificationType NotificationType;
            public NotificationStatus NotificationStatus;
            
            public AlertNotificationResponse(String TId, String notifyType, String mailStat, String mailErr){
//                TransactionId = new TransactionId(TId);
//                NotificationType = new NotificationType(notifyType);
                TransactionId = TId;
                NotificationStatus = new NotificationStatus(mailStat, mailErr);
            }
        }
    
    @HttpPost
    global static AlertNotificationResponse AlertNotification() {
        User users;
        LCBS_RequestResponse_Log__c req = new LCBS_RequestResponse_Log__c();
    
        RestRequest restreq = RestContext.request;
        Blob restbody = restreq.requestBody;

        String reqString;
        String Receiver;
        String Subject;
        String MessageBody;
        String SMSBody;
        String CreatedDate;
        String AlertCode;
        String Priority;
        String BuildingId;
        String CloudAlertId;
        
        AlertNotificationResponse response;
        
        String NotificationType = '';
        String Status = '';
        String ErrMsg = '';
        String TransactionId = '';

        Set<String> UserIdSet = new Set<String>();
        
        reqString = restbody.toString();
        map<String, Object> reqbody = (map<String, Object>)JSON.deserializeUntyped(reqString);
        List<Object> UserIds = (List<Object>)reqbody.get('UserIds');
        BuildingId = (String)reqbody.get('BuildingId');
        CloudAlertId = (String)reqbody.get('CloudAlertId');
        Priority = (String)reqbody.get('Priority');
        CreatedDate = (String)reqbody.get('AlertStartDate');
        map<String,Object> Email = (map<String,Object>)reqbody.get('Email');
        Subject = (String)Email.get('Subject');
        MessageBody = (String)Email.get('MessageBody');
        map<String,Object> SMS = (map<String,Object>)reqbody.get('Sms');
        SMSBody = (String)SMS.get('MessageBody');
        
        System.debug('Request Body:::'+reqbody);
        System.debug('User Ids in Request:::'+UserIds);
        
        List<Id> HPuserIds = new List<Id>();
        
        for(Object UsrId:UserIds) {
//              map<String, Object> u = (map<String, Object>)UsrId;
//              UserIdSet.add((String)u.get('UserId'));
              UserIdSet.add((String)UsrId);
             
        }
        
        Map<String,User> UsersMap = new Map<String,User>([SELECT Id, Contact.High_Priority__c, Contact.Medium_Priority__c, Contact.Low_Priority__c, Contact.Email, Contact.Phone_Converted__c FROM User WHERE Id IN :UserIdSet]);
        System.debug('Users Map:::'+UsersMap);
        System.debug('Chiru1::::::::::::::You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
        for (String uId:UserIdSet) {
            users = UsersMap.get(uId);
            if((Priority == 'High' && users.Contact.High_Priority__c =='Email')||(Priority == 'Medium' && users.Contact.Medium_Priority__c == 'Email')||(Priority == 'Low' && users.Contact.Low_Priority__c == 'Email'))
            {
                HPuserIds.add(uId);
              
                }
                }
                
                  try {
                        //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
                        mail.setTargetObjectIds(HPuserIds);
                        mail.setsaveAsActivity(false);
                        mail.setSenderDisplayName('Building Notification');
                        //mail.setSubject(Subject);
                        string emailMessage = MessageBody;
                        mail.setTemplateID('00Xg0000000ECHM');
                       // mail.setHtmlBody(emailMessage);
                        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
                        System.debug('Chiru222::::::::::::::You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
                        /*
                        List<String> sendTo = new List<String>();
                        List<Messaging.SingleEmailMessage> sitemails = new List<Messaging.SingleEmailMessage>();
                        Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
                        sendTo.add(users.Contact.Email);
                        //sitemail.settargetObjectId(users.Contact.id);
                        sitemail.setToAddresses(sendTo);
                        //System.debug('@@@@@@'+sendTo);
                        sitemail.setSenderDisplayName('Building Notification');
                        sitemail.setSubject(Subject);
                        sitemail.setSaveAsActivity(true);
                        string emailMessage = MessageBody;
                        sitemail.setHtmlBody(emailMessage);
                        sitemails.add(sitemail);
                        //System.debug('$$$$$$'+sitemails);
                        Messaging.sendEmail(sitemails);
                        */
                        NotificationType = 'Email';
                        Status = 'Success';
                        ErrMsg = '';
                } catch (Exception ex){
                    system.debug(ex.getMessage());
                    NotificationType = 'Email';
                    Status = 'Failed';
                    ErrMsg = ex.getMessage();
                 }
            
            for (String uId:UserIdSet) {
            if((Priority == 'High' && users.Contact.High_Priority__c =='SMS')||(Priority == 'Medium' && users.Contact.Medium_Priority__c == 'SMS')||(Priority == 'Low' && users.Contact.Low_Priority__c == 'SMS'))
            {
                //System.debug('Entering now @@@@@@');
                  try {
                        //System.debug('@@@@@@'+SMSBody);
                        //System.debug('&&&&&&'+users.Contact.Phone_Converted__c);                        
                        EmailSMSCallout.basicEmailSMSCallout(users.Contact.Phone_Converted__c,SMSBody);
                        NotificationType = 'SMS';
                        Status = 'Success';
                        ErrMsg = '';
                } catch (Exception ex){
                    system.debug(ex.getMessage());
                    NotificationType = 'SMS';
                    Status = 'Failed';
                    ErrMsg = ex.getMessage();
                 }
            }
            
            req.Name = req.id;
            req.Request__c = reqString;
            req.Response__c = NotificationType +'\n'+Status +'\n'+ErrMsg;
        }
        insert req;
        response = new AlertNotificationResponse(req.id,NotificationType,Status,ErrMsg);
        return response; 
    } 
    }