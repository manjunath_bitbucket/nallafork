/************************************
Name         : LCBS_Allbuidings_controller
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  : Nagarajan Varadarajan
***********************************/

public without sharing class LCBS_Allbuidings_controller{
    public List<Building__c> build {get;set;}
    public String buildingName {get;set;}
    public List<Building__c> BuildingGUID {get;set;}
    public List<User> users {get;set;}
    public user u {get;set;}
    public boolean isDistributor {get;set;}
    public string buildId {get;set;}
    public String PAGE_CSS { get; set; }
    public LCBS_Allbuidings_controller() {
        BuildingGUID = new List<Building__c>();
          users = new List<User>();
          u = [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
          Contact c = [select id from Contact where id=:u.Contactid];
          system.debug('contact list'+c.id);
          system.debug('User ID'+U.id);
          system.debug('User Contact Tes:'+u.contactid);
          system.debug('User Contact Tes:'+u.Account.EnvironmentalAccountType__c);
         if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }
          
        PAGE_CSS = LCBSParameters.PAGE_CSS_URL;


        List<Account> acc = [Select id,Name FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =: u.AccountId)];
        List<Contact> con = [Select id from Contact where AccountId =:acc];
        con.add(c);
         build = [Select Id,Name,Address__c,Address_1__c,Address_2__c,BuildingGUID__c, Alternate_Dispatcher__c,Alternate_Technician__c,Building_Owner__c,City__c,Contractor_Company_Location__c,Country__c,
                 Dispatcher__c,Building_Owner__r.name,Email_Address__c,First_Name__c,Last_Name__c,Building_Owner__r.id,
                 Number_of_Devices__c,Dispatcher__r.name,Technician__r.name,Phone_Number__c,Postal_Code__c,Site__c,
                 State__c,Technician__c,Time_Zone__c,Title__c from Building__c where Alternate_Dispatcher__c =:con OR Alternate_Technician__c=:con OR Dispatcher__c =:con OR Technician__c=:con];      
        
        BuildingGUID = [Select Id,Name,BuildingGUID__c from Building__c where Name=:buildingName];       
    }
    
    public pagereference newBuildingOwner(){
        
        pagereference p = new pagereference('/apex/LCBSNewBuildingOwner'); 
        p.setredirect(true);
        return p; 
    }
    public pagereference newBuilding(){
        
        pagereference p = new pagereference('/apex/LCBS_BuildingInformation'); 
        p.setredirect(true);
        return p; 
    }
    
    public pageReference moreinfo()
    {
       Building__c b = [Select id,name,Building_Owner__c from Building__c where id=:buildId];
       if(b.Building_Owner__c == NULL)
            {
             PageReference p = new pageReference('/LCBS_MoreInfo_BuildInformation?Id='+buildId);
             p.setRedirect(true);
             return p;
            }
            else
            {
             PageReference p1 = new pageReference('/LCBS_MoreInfo_BuildInfo?Id='+buildId);
             p1.setRedirect(true);
             return p1;
            }
    }
}