/************************************
Name         : distributorInvite
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class distributorInvite{

    public static id contractor{get; set;}
    public static id distributor{get; set;}
    public contact cc{get; set;}
    public String PAGE_CSS{get; set;}
    public user u{get; set;}
    public Account c{get; set;}
    public document sign{get; set;}
    public document logo{get; set;}
    String Env;
    public string executive_name{get; set;}
    public string executive_email{get; set;}
    public string executive_phone{get; set;}
    public string url{get; set;}
    public string orgid{get; set;}
    public string sfdcBaseURL{get; set;}
    public String d {get;set;}
    public distributorInvite(){
        d= DateTime.now().format('MMMMM dd, yyyy');
        sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
        orgid= UserInfo.getOrganizationId();
        sign = [select id,name from document where name='Honeywell vicepresident sign'];
        logo = [select id,name from document where name='LCBS_Email_Logo'];
        cc = [select id, name, email, firstname, lastname, Account.Name, Account.Email__c, Account.Phone, Account.Account_Exec_Name__c, Account.Account_Exec_Email__c, Account.Account_Exec_Phone__c from contact where id=:contractor];
        // Account.Account_Executive_Info__c
        // system.debug('Invited Contacts'+cc);
        u = [select id, name, federationIdentifier, contactid, Contact.AccountId, Contact.FirstName, Contact.Lastname, Contact.Name, Contact.Phone, Contact.Email, contact.account.email__c, contact.account.phone, contact.account.name from user where id=:distributor];
        // system.debug('Distributor Contact'+u);
        // c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
        //system.debug('%%'+c.contacts[0].firstname);
        
        if(cc.Account.Account_Exec_Name__c != NULL){
            executive_name = cc.Account.Account_Exec_Name__c;
        }
        else{
            executive_name = '';
        }
        
        if(cc.Account.Account_Exec_Email__c != NULL){
            executive_email = cc.Account.Account_Exec_Email__c;
        }
        else{
            executive_email = '';
        }
        
        if(cc.Account.Account_Exec_Phone__c != NULL){
            executive_phone = cc.Account.Account_Exec_Phone__c;
        }
        else{
            executive_phone = '';
        }
        
        /* if(cc.Account.Account_Executive_Info__c != null){
            // String[] exec = cc.Account.Account_Executive_Info__c.split('\\|');
            executive_name = cc.Account.Account_Exec_Name__c;
            executive_email = cc.Account.Account_Exec_Email__c;
            executive_phone = cc.Account.Account_Exec_Phone__c;
        }
        
        else{
            executive_name = '';
            executive_email = '';
            executive_phone = '';
        } */
        
        //PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
    }

}