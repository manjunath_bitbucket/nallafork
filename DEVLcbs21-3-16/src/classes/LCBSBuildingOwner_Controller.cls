public class LCBSBuildingOwner_Controller{
    public Building_Owner__c BuldENew {get;set;}
    public List<Building_Owner__c> BuildingOwners{get;set;}
    public Building__c build {get;set;}
     public string Address1 {get;set;}
    public string Address2 {get;set;}
    public string Address3 {get;set;}
    public Building_Owner__c building {get;set;}
    public user u {get;set;}
    public String BuildingId {get;set;}
    public boolean showAndHide {get;set;}
    public Id currentBOwner {get;set;}
    public string currentBOwnerEmail {get;set;}
    public transient List<Messaging.SingleEmailMessage> mails {get;set;}
    public user currentuser {get;set;}
    public LCBSBuildingOwner_Controller(ApexPages.StandardController controller) {
       BuildingId = System.currentPageReference().getParameters().get('Id');
       currentuser = [select id,name,contact.Role__c from user where id=:userinfo.getuserid()];
        showAndHide = false ; 
        BuldENew = new Building_Owner__c();
   build=[select id,name,Building_Owner__c,Building_Owner__r.Email_Address__c,Building_Owner__r.Phone_Number__c,createddate,createdBy.id from Building__c where id=:BuildingId];
        system.debug('###building'+BuildingId );
     u = [select id,name,accountid,Account.Name,Contactid from user where id=:build.createdby.id];
       // PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
  BuildingOwners = [Select id,Name,CreatedBy.id,Address__c,Address_1__c,Customer_Type__c,Address_2__c,City__c,Contractor_Branch_Location__c,Company_Company_Location12__r.name,Company_Company_Location12__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c ];
           setNone(True);
        setNew(False);
        setExist(False);
       BuldENew.Country__c ='--Select--';
       BuldENew.Customer_Type__c='Standard';
    }


        public void displaySec(){
        showAndHide =true;
        }
        
        public void displaySec2(){
        showAndHide =false;
        }

    public pagereference AssignBuildingOwner()
    {
     build.Building_Owner__c = currentBOwner;
     build.EULA_Agreement__c = true;
     update build;
     
     sendEmail();
     Pagereference page = new PageReference('/LCBS_MoreInfo_BuildInfo?id='+BuildingId);
     page.setRedirect(true); 
     return page;
     
    }
    

    public list<Building_Owner__c> lstBuildingOwner {get;set;}
    public string sBuildOwner{get;set;}
    public List<SelectOption> lstExtOwn {get;set;}
    String TabInFocus = System.currentPageReference().getParameters().get('tab');
    //BuildingId = System.currentPageReference().getParameters().get('Id');
   // public String PAGE_CSS { get; set; }


    public LCBSBuildingOwner_Controller(){
        lstBuildingOwner = new list<Building_Owner__c>();
        
    }
    
        
    public List<SelectOption>  getBuildingOwners(){
        lstBuildingOwner = [Select id, Name,First_Name__c,Last_Name__c from Building_Owner__c];
        system.debug('lstBuildingOwner++++'+lstBuildingOwner);
        List<SelectOption> BuildingOwners= new List<SelectOption>();
        for(Building_Owner__c s:lstBuildingOwner){
            
            BuildingOwners.add(new SelectOption(s.id,s.First_Name__c+' '+ s.Last_Name__c+', '+s.Name));
        }        
        return BuildingOwners;
    }
    
    public String getTabInFocus() {
         System.debug(' *****Current Selected tab is :' + TabInFocus);
    
    return TabInFocus;
    }
    
    public void setTabInFocus( String s ) {
        this.TabInfocus = s;
    }
    Private String var = 'photos';
    public void setVar(String n) {
        var = n;
    }
    
    public String getVar() {
        return var;
    }
    public String setActiveTab(){
            String para = ApexPages.CurrentPage().getParameters().get('var');        
            System.debug('current tab is ' + para);
            return para;
        }
        
      public PageReference buildingOwnerSave() {
       Boolean isTrue = false;
        try{   
           /* if(Address1!=NULL)
            {
             Address1 = Address1+', ';
            }
            if(Address2!=NULL && Address3!=NULL)
            {
             Address2 = Address2+', ';
            }
            */
            BuldENew.Address__c = Address1+' '+Address2+' '+Address3;
            if(BuldENew != Null){  
                try{
                insert BuldENew;
               // LCBSBuildingCallout.getBuildings(build.Name,build.Id, BuldENew.id, BuldENew.Name);
                isTrue = true;
                
                }catch(Exception e){} 
            }
           // BuldENew.Address_1__c = 'test';
           // update BuldENew;
            build.Building_Owner__c = BuldENew.Id;
            update build;
            system.debug('Naga Test###New Building owner'+BuldENew);
           if(isTrue == true){
           sendEmail();       
             }
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
            
        }   
       
        if( BuldENew != null){  
         system.debug('>>>>>>>>>');  
       // update BuldENew;   
      //  sendEmail(); 
            }    
        //sendEmail();  
        pagereference p = new pagereference('/LCBS_MoreInfo_BuildInfo?id='+BuildingId); 
        p.setredirect(true);
        return p;                                               
    }   
    
     public void sendEmail(){
        try{
         EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
            //Mail Functionality added by Chiranjeevi
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
           Building_Owner__c bo =[select id,Email_Address__c from Building_Owner__c where id=:currentBOwner];
           
            system.debug(bo);
            Contact c = [select id, Email from Contact where email <> null limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
             sendTo.add(bo.Email_Address__c );
            system.debug('Email address###:'+bo.Email_Address__c);
           // system.debug('Email address@@@@@@@@@:'+BuldENew.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(u.contactid);           
            mail.setSenderDisplayName('LCBS Building Approval');
            mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+build.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            system.debug('Building ID ^^^^^^^:'+build.id);
            mail.whatid = build.id;
            mails.add(mail);
            system.debug('mails ########:'+mails);
            Savepoint sp = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
           
            
         
            
            /*
            Task task = new Task();
            task.WhatId = build.id;
           // task.WhoId = BuldENew.Technician__c;
            task.Subject = 'Email Sent to Building Owner';
            task.status = 'Completed';
           // task.Description ='TIME :   '+system.now() + '\r\n'+ '\r\n' + 'APPROVER DETAILS' + '\r\n' + 'NAME :  ' + build.Building_Owner__r.name +'\r\n'+ 'EMAIL :  ' + build.Building_Owner__r.Email_Address__c + '\r\n' + ' PHONE NUMBER :   ' + build.Building_Owner__r.Phone_Number__c;
            task.ActivityDate = Date.today();
            insert task;  
            */
            }
            }
            catch(Exception e)
            {
             system.debug(e);
            }
       }     
           
           
           
            public String country { get; set; }    
    
    //To display list of picklist values on VF page
    public List<selectOption> getPicklistvalues() {
        List<selectOption> options = new List<selectOption>();       
        options.add(new selectOption('--None--','--None--'));
        options.add(new selectOption('Assign an existing building owner','Assign an existing building owner'));
        options.add(new selectOption('Add a new building owner','Add a new building owner'));
        return options;
    } 
    
    
    public Boolean nonetf = false;
    public Boolean existingtf = false;
    public Boolean newtf = false;
       
    //To dynamically pass Boolean values to rendered attribute on pageblocksection
    public void setNone(Boolean b) {
        this.nonetf = b;
    }
    public Boolean getNone() {
        return this.nonetf ;
    }    
    
    
    public void setExist(Boolean b) {
        this.existingtf = b;
    }
    public Boolean getExist() {
        return this.existingtf ;
    }

    
    public void setNew(Boolean b) {
        this.newtf = b;
    }
    public Boolean getNew() {
        return this.newtf ;
    }
 
    //Constructor, After loading the page to display india pageblocksection by default
    
    //After changing picklist value based on the selection to display either usa or aus pageblocksection
    //Through actionfunction or actionsupport this method will be called to VF page
    public PageReference selectcountry() {
        if(country == 'Assign an existing building owner') {
            setExist(True);
            setNew(False);
        }
        else {
            setExist(False);
            setNew(True);
        }
        return null;
    }  


   
  
           
    public PageReference buildingOwnerCancel() {
        pageReference p = new pageReference('/LCBS_MoreInfo_BuildInfo?id='+BuildingId);
        p.setRedirect(true);
        return p;                                               
    }     
    
    public void EbuildingOwnerSave()
    {
      
    
}
public pagereference equipmentPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/equipments/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}

public pagereference alertsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 // string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/alerts/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pagereference controlsPage()
{    
// String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/controls/'+u.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
}